FROM openjdk:11-jdk

RUN mkdir -p /app/bin && \
    mkdir -p /app/jenkins_home && \
    mkdir -p /app/jenkins && \
    wget https://repo.jenkins-ci.org/releases/io/jenkins/jenkinsfile-runner/jenkinsfile-runner/1.0-beta-10/jenkinsfile-runner-1.0-beta-10.jar -O /app/bin/jenkinsfile-runner.jar

COPY jenkins_home/ /app/jenkins_home
COPY jenkins.war /app/bin/jenkins.war
COPY jenkinsfile-runner /app/bin/jenkinsfile-runner
COPY Test-Jenkinsfile /tmp/Jenkinsfile

ENV JENKINS_HOME /app/jenkins_home 
RUN unzip /app/bin/jenkins.war -d /app/jenkins && \
    ln -s /app/bin/jenkinsfile-runner /usr/bin/jenkinsfile-runner && \
    chmod +X /usr/bin/jenkinsfile-runner && \
    chmod 777 /usr/bin/jenkinsfile-runner


CMD ["/bin/bash"]
